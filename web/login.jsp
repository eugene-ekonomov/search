<%--
  Created by IntelliJ IDEA.
  User: X
  Date: 27.08.2018
  Time: 17:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
    <style>
        .ctr{
            margin: auto;
            left: 40%;
            position: absolute;
            top: 40%;
        }
        input{
            margin-bottom: 10px;
        }
        .btn{
            display: block;
            margin: auto;
        }
    </style>
</head>
<body>
<form class="ctr" method="POST" action="j_security_check">
    Username: <input type="text" name="j_username"> <br/>
    Password : <input type="password" name="j_password"> <br/>
    <input class="btn" type="submit" value="Login">
</form>
</body>
</html>
