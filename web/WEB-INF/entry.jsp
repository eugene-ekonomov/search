<?xml version="1.0" encoding="UTF-8"?>
<%@page contentType="application/xml" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<data>
    <table>
        <tr>
            <th>Город</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Модель машины</th>
            <th>Номер машины</th>
        </tr>
        <c:forEach items="${entryList}" var="entry">
            <tr>
                <td>${entry.city}</td>
                <td>${entry.lastName}</td>
                <td>${entry.firstName}</td>
                <td>${entry.fathersName}</td>
                <td>${entry.carModel}</td>
                <td>${entry.carNumber}</td>
            </tr>
        </c:forEach>
    </table>
</data>