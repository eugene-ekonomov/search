<%--
  Created by IntelliJ IDEA.
  User: X
  Date: 24.08.2018
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Search</title>
  <link rel="stylesheet" type="text/css" href="CSS/search.css">
</head>
<body>

  <section id="main">
    <a href="logout.jsp" >Выход из аккаунта </a>
    ${pageContext.request.remoteUser}

    <div id="filters">
        <div id = "searchLabel">Поиск</div>
        
        <div class="text">Город</div>
        
        <input id='city' type='text' />
        
        <div class="text">Фамилия</div>
        
        <input id='lastName' type='text' />
        
        <div class="text">Имя</div>
        
        <input id='firstName' type='text' />
        
        <div class="text">Отчество</div>
        
        <input id='fathersName' type='text' />
        
        <div class="text">Модель автомобиля</div>
        
        <input id='model' type='text' />

        <input id="button" type="button" value="Поиск"/>
      </div>
      <br /><br />
      <div id='result'> </div>

  </section>


  <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="scripts/search.js"></script>


</body>
</html>
