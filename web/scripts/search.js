$("#button").on('click', function() {
    var city=document.getElementById('city').value;
    var lastName=document.getElementById('lastName').value;
    var firstName=document.getElementById('firstName').value;
    var fathersName=document.getElementById('fathersName').value;
    var model=document.getElementById('model').value;

    if(city=="" && lastName=="" && firstName=="" && fathersName=="" && model==""){
        $("#result").html("<div class='text'>Заполните хотя-бы одно поле для поиска</div>");
        return;
    }

    $.ajax({
        method: "GET",
        url: "search",
        data: "city="+city+"&lastName="+lastName+"&firstName="+firstName+"&fathersName="+fathersName+"&model="+model, // если сервер что-то получает
        success: function (responseXml) {
            $("#result").html($(responseXml).find("data").html()); // Parse XML, find <data> element and append its HTML to HTML DOM element with ID "somediv".

        }
    });
});