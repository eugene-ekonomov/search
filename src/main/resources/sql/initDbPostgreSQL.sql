DROP TABLE IF EXISTS city CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE IF EXISTS car_type CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS user_roles CASCADE;
DROP SEQUENCE IF EXISTS global_city_seq;
DROP SEQUENCE IF EXISTS global_person_seq;
DROP SEQUENCE IF EXISTS global_car_seq;
DROP SEQUENCE IF EXISTS global_car_type_seq;

CREATE SEQUENCE global_city_seq START 1;
CREATE SEQUENCE global_person_seq START 1;
CREATE SEQUENCE global_car_seq START 1;
CREATE SEQUENCE global_car_type_seq START 1;

CREATE TABLE city
(
  id INTEGER PRIMARY KEY DEFAULT nextval('global_city_seq'),
  name VARCHAR NOT NULL
);

CREATE TABLE person
(
  id    INTEGER PRIMARY KEY DEFAULT nextval('global_person_seq'),
  last_name VARCHAR NOT NULL,
  first_name  VARCHAR NOT NULL,
  father_s_name VARCHAR,
  city_id INTEGER NOT NULL,
  FOREIGN KEY (city_id) REFERENCES city(id),
  registered TIMESTAMP DEFAULT now()
);
CREATE INDEX person_idx ON person(last_name);

CREATE TABLE car_type
(
  id INTEGER PRIMARY KEY DEFAULT nextval('global_car_seq'),
  name VARCHAR NOT NULL
);

CREATE TABLE car
(
  id INTEGER PRIMARY KEY DEFAULT nextval('global_car_type_seq'),
  number VARCHAR NOT NULL,
  person_id INTEGER NOT NULL,
  car_type_id INTEGER NOT NULL,
  FOREIGN KEY (person_id) REFERENCES person(id),
  FOREIGN KEY (car_type_id) REFERENCES car_type(id)
);

create table users (
  user_name         varchar(15) not null primary key,
  user_pass         varchar(15) not null
);

create table user_roles (
  user_name         varchar(15) not null,
  role_name         varchar(15) not null,
  primary key (user_name, role_name)
);