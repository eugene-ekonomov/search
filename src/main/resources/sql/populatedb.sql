DELETE FROM car;
DELETE FROM person;
DELETE FROM car_type;
DELETE FROM city;
ALTER SEQUENCE global_city_seq RESTART WITH 1;
ALTER SEQUENCE global_person_seq RESTART WITH 1;
ALTER SEQUENCE global_car_seq RESTART WITH 1;
ALTER SEQUENCE global_car_type_seq RESTART WITH 1;


INSERT INTO city(name) VALUES
  ('Москва'),
  ('Санкт-Петербург'),
  ('Екатеринбург'),
  ('Новосибирск'),
  ('Тюмень'),
  ('Ханты-Мансийск'),
  ('Калининград'),
  ('Ревда'),
  ('Ирбит'),
  ('Омск');

INSERT INTO person (last_name, first_name, father_s_name, city_id) VALUES
  ('Москвин', 'Московий', 'Московиевич', 1),
  ('Петров', 'Пётр', 'Петрович', 2),
  ('Екатеринова', 'Екатерина', '', 3),
  ('Сибиряков', 'Сибиряк', 'Сибирякович', 4),
  ('Тюменцев', 'Тюмен', 'Тюменович', 5),
  ('Хантов', 'Хант', 'Хантович', 6),
  ('Калинин', 'Калин', 'Калинович', 7),
  ('Ревдин', 'Ревд', 'Ревдович', 8),
  ('Ирбитова', 'Ирбита', 'Ирбитовна', 9),
  ('Омсков', 'Омск', 'Омскович', 10);

INSERT INTO car_type(name) VALUES
  ('Mazda 6'),
  ('Volvo S80'),
  ('Lada Kalina'),
  ('KIA Spectra');

INSERT INTO car(number, person_id, car_type_id) VALUES
  ('Н001ОМ 999', 1, 1),
  ('Н002ОМ 999', 2, 1),
  ('Н003ОМ 999', 3, 1),
  ('Н004ОМ 999', 4, 2),
  ('Н005ОМ 999', 5, 2),
  ('Н006ОМ 999', 6, 2),
  ('Н007ОМ 999', 7, 3),
  ('Н008ОМ 999', 8, 3),
  ('Н009ОМ 999', 9, 3),
  ('Н010ОМ 999', 10, 3),
  ('Н011ОМ 999', 1, 3),
  ('Н012ОМ 999', 2, 3),
  ('Н013ОМ 999', 3, 3),
  ('Н014ОМ 999', 4, 4),
  ('Н015ОМ 999', 5, 4),
  ('Н016ОМ 999', 6, 4),
  ('Н017ОМ 999', 7, 4),
  ('Н018ОМ 999', 8, 4),
  ('Н019ОМ 999', 9, 4);

INSERT INTO users(user_name, user_pass) VALUES
  ('user', 'user'),
  ('admin', 'admin'),
  ('user2', 'user2');

INSERT INTO user_roles(user_name, role_name) VALUES
  ('user', 'user'),
  ('admin', 'admin'),
  ('user2', 'user');