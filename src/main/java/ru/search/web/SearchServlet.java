package ru.search.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.search.model.Entry;
import ru.search.repository.JDBCRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;



@WebServlet("/search")
public class SearchServlet extends HttpServlet {

    static final Logger log = LogManager.getLogger(SearchServlet.class);

    private JDBCRepository repository;

    public void init(ServletConfig config) throws ServletException {
        repository = new JDBCRepository();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String city = request.getParameter("city");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String fathersName = request.getParameter("fathersName");
        String model = request.getParameter("model");
        ArrayList<Entry> entryList = repository.getData(lastName, firstName,fathersName,city,model);

        log.info(entryList.toString());
        request.setAttribute("entryList", entryList);
        request.getRequestDispatcher("/WEB-INF/entry.jsp").forward(request, response);

    }
}


