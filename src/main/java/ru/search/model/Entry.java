package ru.search.model;

public class Entry {
    private String lastName;
    private String firstName;
    private String fathersName;
    private String city;
    private String carModel;
    private String carNumber;

    public Entry(String lastName, String firstName, String fathersName, String city, String carModel, String carNumber) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.fathersName = fathersName;
        this.city = city;
        this.carModel = carModel;
        this.carNumber = carNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getCity() {
        return city;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarNumber() {
        return carNumber;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", fathersName='" + fathersName + '\'' +
                ", city='" + city + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carNumber='" + carNumber + '\'' +
                '}';
    }
}
