package ru.search.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.search.model.Entry;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;


public class JDBCRepository {
    private static final Logger log = LogManager.getLogger(JDBCRepository.class);
    private DataSource ds;

    public JDBCRepository(){
        HikariConfig config = new HikariConfig("/sql/postgres.properties");
        ds = new HikariDataSource(config);
        log.info("Подключение к базе данных: " + config.getDataSourceProperties().getProperty("serverName"));
    }
    public ArrayList<Entry> getData(String lastName, String firstName, String fathersName, String city, String model){
        try (Connection conn = ds.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement(
                        " select    prs.last_name last_name,\n" +
                                "       prs.first_name first_name,\n" +
                                "       prs.father_s_name father_s_name,\n" +
                                "       ct.name city_name, \n" +
                                "       ctr.name car_model,\n" +
                                "       cr.number car_number\n" +
                                "from city ct\n" +
                                "join person prs on ct.id = prs.city_id\n" +
                                "join car cr on prs.id = cr.person_id\n" +
                                "join car_type ctr on cr.car_type_id = ctr.id\n" +
                                "where (prs.last_name like ? or ? is null)\n" +
                                "and (prs.first_name like ? or ? is null)\n" +
                                "and (prs.father_s_name like ? or ? is null)\n" +
                                "and (ctr.name like ? or ? is null)\n" +
                                "and (ct.name like ? or ? is null)");
            preparedStatement.setString(1, lastName+"%");
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, firstName+"%");
            preparedStatement.setString(4, firstName);
            preparedStatement.setString(5, fathersName+"%");
            preparedStatement.setString(6, fathersName);
            preparedStatement.setString(7, model+"%");
            preparedStatement.setString(8, model);
            preparedStatement.setString(9, city+"%");
            preparedStatement.setString(10, city);

            ResultSet result = preparedStatement.executeQuery();

            ArrayList<Entry> entries = new ArrayList<>();
            while(result.next()){
                entries.add(new Entry(result.getString(1),
                                      result.getString(2),
                                      result.getString(3),
                                      result.getString(4),
                                      result.getString(5),
                                      result.getString(6) ));
            }

            result.close();
            preparedStatement.close();

            return entries;
        }catch (SQLException ex){
            log.error("SQLException: " + ex.getMessage());
        }
        return null;
    }
}
